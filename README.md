Unis

by David Heitzman, 2013

This project is a simple page for viewing the entire unicode character set, at least those characters supported by your browser. 

* js will start adding rows of characters at the bottom of the document. It takes a while for them to all load, but on the plus side, the page loads immediately in your browser.
* Hover over any character. The last 3 will be shown in large size at the top of the document. 
* Click on any character. The last two characters you have clicked will stick at the top of the document for viewing. 


This page was created from the HTML5 and canvas boilerplate app: 

[HTML5-Canvas-Game-Boilerplate](https://github.com/IceCreamYou/HTML5-Canvas-Game-Boilerplate).
