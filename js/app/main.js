update = function(){
  // $("#term").html($("#term").html()+"<br />"+window.t);
  // $("#term").html(window.t);
};

function randColor(){
  var hex_letters = '123456789ABCDEF';
  var p=Math.random()*hex_letters.length;
  var out="#";
  for (var c=0;c<6; c++){
    p=Math.random()*hex_letters.length;
    out += hex_letters.slice(p,p+1);
  }
  return out;
};

statifyText=function(){
  var text=$("#input_text").html();
  console.log(text);
  var tsa=text.split(/\s+/);
  $.each(tsa, function(ind,word){
    $("p#term").html(word);
  });
  var last='';
  wordFreq={};
  for(var c=tsa.length-1;c>=0;c--) {
    $("p#term").html(last=tsa[c]);
    if (wordFreq[ tsa[c] ]){
      wordFreq[ tsa[c] ] += 1;
    }else{
      wordFreq[ tsa[c] ] = 1;
    }
  }
  // wordFreq=wordFreq.sort(function(w1,w2){} )
  wfA=[]
  $.each(wordFreq, function(k,v){
    wfA.push([k,v]);
  })
  wfA= wfA.sort(function(a,b){ return b[1] - a[1] ;});
  $("p#term").html(last);
};

function onLoad(){
  // $("#input_text").load("starwars.txt",
  //   function(){
  //     window.statifyText();
  //   });
  $("p#term").html("Like a terminal");
  var vertices = [];
  for (var xc=0;xc < 100; xc++){
    vertices.push([Math.random(200),Math.random(200)]);
  }
  window.voronoi=d3.geom.voronoi(vertices);
  w=800;
  h=600;
    svg_element=d3.select("#d3_area1").
    append("svg").
    attr("width",w).
    attr("height",h);

    svg_element.
    append("circle").
    style("stroke","grey").
    style("fill",randColor).
    attr("r",50).
    attr("cx",80).
    attr("cy",80);

    var lg=svg_element
      .append("g")
      .attr("class","line_group");
    lg.append("line")
      .attr("stroke","#ff7765")
      .attr("stroke-width","5")
      .attr("x2",25)
      .attr("y2",25)
      .attr("x1",548)
      .attr("y1",478)
      ;
   $( "#slider-max_branch" ).slider({
      value: 4,
      min: 1,
      max: 10,
      slide: function( event, ui ) {
        $( "#max_branch" ).val( ui.value );
      }
    });
   $( "#slider-stroke_shrink" ).slider({
      value:50,
      min: 0,
      max: 99,
      slide: function( event, ui ) {
        $( "#stroke_shrink" ).val( ui.value );
      }
    });
   $( "#slider-length_shrink" ).slider({
      value:50,
      min: 0,
      max: 99,
      slide: function( event, ui ) {
        $( "#length_shrink" ).val( ui.value );
      }
    });
   $( "#slider-new_theta_range" ).slider({
      value:50,
      min: 0,
      max: 99,
      slide: function( event, ui ) {
        $( "#new_theta_range" ).val( ui.value );
      }
    });
   $( "#slider-color_lightening" ).slider({
      value:50,
      min: 0,
      max: 99,
      slide: function( event, ui ) {
        $( "#color_lightening" ).val( ui.value );
      }
    });
   $( "#slider-branch_length_default" ).slider({
      value:50,
      min: 0,
      max: 99,
      slide: function( event, ui ) {
        $( "#branch_length_default" ).val( ui.value );
      }
    });
   $( "#slider-depth" ).slider({
      value:4,
      min: 1,
      max: 10,
      slide: function( event, ui ) {
        $( "#depth" ).val( ui.value );
      }
    });
    console.log(svg_element);
}

function circle_shrink_move(){
  svg_element=d3.select("#d3_area1").
    selectAll("circle").
    style("stroke","grey").
    style("fill","green").
    attr("r",50).
    attr("cx",80).
    attr("cy",80).
    on("mouseover", function(){ d3.select(this).style("fill","aliceblue");}).
    on("mouseout", function(){ d3.select(this).style("fill","green"); }).
    transition().
    delay(100).
    duration(5000).
    attr("r",10).
    attr("cx",300).
    attr("cy",400).
    style("fill","orange");
}

function circle_move_random(){
  svg_element=d3.select("#d3_area1").
    selectAll("circle").
    attr("cx",Math.random()*200 ).
    attr("cy",Math.random()*200 );


}
function circle_move_random_smooth(){
    svg_element=d3.select("#d3_area1").
    selectAll("circle").
    transition().
    attr("cx",Math.random()*200 ).
    attr("cy",Math.random()*200 ).
    append("text","why?");


}

function circles_for_words(){
  // wfA
  svg_element=d3.select("#d3_area1 svg");
  svg_element.selectAll("circle").remove();
  circle=svg_element.selectAll("circle").
    data(wfA);
    // data([17,14,12,4]);

  circle.enter()
    .append("circle")
    .attr("cx",function(){return Math.random()*700 + 50} )
    .attr("cy",function(){return Math.random()*500 + 50} )
    .style("fill", randColor )
    .attr("stroke",randColor)
    .attr("r",function(a){return (wfA.length/22)*a[1];})
    .each( animate(1.0) )
    ;
}
  
function circle_chart(){
  svg_element=d3.select("#d3_area1 svg");
  svg_element.selectAll("circle").remove();
  circle=svg_element.selectAll("circle")
    .data(wfA.slice(0,19) );

  var h = svg_element.attr("height");
  var w = svg_element.attr("width");
  
  var x = d3.scale.linear().domain([0,19]).range([ 0, w ]);
  var y = d3.scale.linear().domain([1,wfA[0][1]]).range([ 60, h ]);
  
  circles=circle.enter()
    .append("circle")
    .attr("r", 15)
    .style("fill",randColor)
    .attr("stroke","#dcab44")
    .attr("cx",function(d,i){return x(i)} )
    .attr("cy",function(d,i){return h- y(d[1])})
  ;
  circles=circle.enter()
    .append("rect")
    .style("fill",randColor)
    .attr("stroke","#dcab44")
    .attr("x", function(d,i){ return x(i); })
    .attr("y", 5 )
    .attr("height",function(d,i){return y(d[1]);})
    .attr("width", 10)
  ;
  circle.enter().
    append("svg:text")
    .attr("color", "black")
    .attr("x", function(d,i){ return x(i); })
    .attr("y", h-5 )
    // .attr("x", 200 )
    // .attr("y", 200 )
    // .attr("dy", "0.35em")
    // .attr("dx", -5 )
    .text(function(d,i){return wfA[i][0];})
    .attr("text-anchor", "middle" )
    ;
} 

function a_path(){
  svg_element=d3.select("#d3_area1 svg");
  svg_element.selectAll("circle").remove();

  var h = svg_element.attr("height"),
      w = svg_element.attr("width");
  
  var x = d3.scale.linear().domain([0,19]).range([ 0, w ]),
      y = d3.scale.linear().domain([1,wfA[0][1]]).range([ 60, h ]);
  
  var starting_points = [[200,200], [200,300], [400,300] ];

  var drag_handler = d3.behavior
    .drag()
    .origin(null)
    .on("drag", function(d,i){
      // console.log(d3.event);
      d3.select(this)
      .attr("cx",d3.event.x)
      .attr("cy",d3.event.y) ; 
      console.log(
        svg_element
          .selectAll("circle")
          // .map(function(d,i){return [d.attr('cx'),d.attr('cy')] })
          // .map(function(d,i){return [d.cx,d.cy] })
          .map(function(d,i){return d })
          // .map(function(d,i){return [d3.select(d).attr('cx'),d.attr('cy')] })

        );
    });



  svg_element.selectAll("circle")
    .data(starting_points)
    .enter()
    .append("svg:circle")
    .attr("r",10)
    .attr("cx",function(d,i){ return d[0] })
    .attr("cy",function(d,i){ return d[1] })
    .attr("stroke","2 #ddeeff")
    .attr("fill", randColor)
    .on("mouseover",function(){d3.select(this).attr("stroke","red")})
    .on("mouseout",function(){d3.select(this).attr("stroke","#ddeeff")})
    .call(drag_handler)
    .on("dragend",function(d,i){
      d3.select(this).stroke("2 #ddeeff");
      svg_element
        .append("path")
        .attr("stroke","#FF3f19")
        .attr("fill",randColor)
        .attr("d","M 200 200 Q 250 250 270 400 z")
        .attr("stroke-width","2")
        ;
    })
    // .on("dragend",function(e){d3.select(this).attr("cx",e.x).attr("cy",e.y) ; })
    ;

} 

function a_line(){
  var yoff=20, xoff=0;
  // var svg_element = d3.select("d3_area1 svg");
  // var lg=svg_element.append("g").attr("class","line_group");
  //  //.select("g.line_group")
  //   // .data([[25,25],[542,689]])
  //   // .enter()
  //   // .line()
  // lg.append("line")
  //   .attr("stroke","#787765")
  //   .attr("stroke-width","15")
  //   .attr("x2",65)
  //   .attr("y2",78)
  //   .attr("x1",448)
  //   .attr("y1",678)
  //   ;
  var svg_element=d3
    .select("#d3_area1 svg g.line_group")
    ;

    // svg_element.append("line")
    //   .attr("stroke","#4fd765")
    //   .attr("stroke-width","6")
    //   .attr("x2",125)
    //   .attr("y2",77)
    //   .attr("x1",466)
    //   .attr("y1",578)
    //   // .transform("
      // ;
  d3.select("#d3_area1 svg g.line_group").selectAll("line").remove();
  var rot="rotate("+0+","+(w/2.0)+","+(h-50)+")" ;
  // console.log(rot);
    svg_element.append("line")
      .attr("stroke","#4fd765")
      .attr("stroke-width","20")
      .attr("x2",w/2.0)
      .attr("y2",h-yoff-80)
      .attr("x1",w/2.0)
      .attr("y1",h-yoff)
      .attr('transform',rot)
      ;
    drawBranch(w/2.0,h-yoff-80,0,1);
}
function drawTree(css_selector, depth, x, y) {
  treecolor="#8B5214";
  var selector=css_selector || "#d3_area1 svg";
  var svg_element=d3.select(selector);
  svg_element.selectAll("line").remove();
  var yoff=10;
  var h=svg_element.attr("height");
  var w=svg_element.attr("width");
  var x = x || w/2.0;
  var y = y || h ;
  max_branch=$("#max_branch").val();
  stroke_shrink=$("#stroke_shrink").val(); //branch thinning rate
  stroke_shrink=( 1 - (stroke_shrink/99.001) ) * 0.3 + 0.59;
  length_shrink=$("#length_shrink").val();
  length_shrink=(1-(length_shrink/99.001)) * 0.26 + 0.7399;
  color_lightening=$("#color_lightening").val();
  color_lightening=(color_lightening/99.001) * 0.2 + 0.23;
  new_theta_range=$("#new_theta_range").val();
  new_theta_range=(new_theta_range/99.001) * 60  + 5; // (5..65)
  new_theta_range=[-new_theta_range,new_theta_range];
  depth=$("#depth").val();
  max_level=10;
  stroke_default=14;
  branch_length_default=$("#branch_length_default").val() ;
  branch_length_default=( branch_length_default / 99.001) * 320 + 30 ;
  
  svg_element.append("line")
    .attr("stroke","#8B5214")
    .attr("stroke-width",20*Math.pow(0.9,depth))
    // .attr("stroke-linecap","round")
    .attr("x2",x)
    .attr("y2",y-yoff-80)
    .attr("x1",x)
    .attr("y1",y-yoff)
    ;
    total_branches=0;
    drawBranch(x,y-yoff-80,0,depth);
    $("#total_branches").html(total_branches);
}

function drawBranch(x,y,theta, level){
  var svg_element=d3
    .select("#d3_area1 svg g.line_group");
  var balanced = true;
    
  var min_branch=1;
  if (level > max_level) { level = max_level;}
  if ( level <= 0)  { return };
  new_branches= min_branch + Math.random()*( max_branch - min_branch );
  total_branches += new_branches | 0;
  var new_thetas = [];
  var conv=Math.PI/360.0;
  var rmin=new_theta_range[0],
      rmax=new_theta_range[1];
  var rspread=Math.abs(rmax-rmin);
  var wedge=rspread;

  for(var c=0; c<new_branches; c++){
    if (balanced){
      wedge=rspread/new_branches; 
      rmin=rmin+c*wedge;
      rmax=rmin + wedge;
    }
    var new_theta =  ( rmin + Math.random()*(wedge) );
    if (new_theta < rmin){ new_theta = rmin };
    if (new_theta > rmax){ new_theta = rmax };
    new_theta = ( theta + new_theta) % 360;
    new_thetas.push( new_theta );
  }

  var stroke , branch_length= 0;
  for(var c=0;c<new_branches;c++){
    //draw branches from x,y to endx,endy
    var newx=x,
        newy=y;

    stroke = stroke_default * Math.pow(stroke_shrink,max_level-level) * (Math.random()*0.6 + 0.7);
    branch_length= branch_length_default * Math.pow(length_shrink,max_level-level) * (Math.random()*0.6 + 0.7);
    newx=x-(branch_length*Math.sin(new_thetas[c]*conv)|0);
    newy=y-(branch_length*Math.cos(new_thetas[c]*conv)|0);
    var new_line=svg_element.append("line")
      .attr("x1",x)
      .attr("y1",y)
      .attr("x2",newx)
      .attr("y2",newy)
      .attr("id","zaney")
      // .attr("stroke",randColor)
      .attr("stroke",d3.rgb(treecolor).brighter(color_lightening*(max_level-level) ))
      .attr("stroke-width",stroke)
      .attr("stroke-linecap","round");
    drawBranch( newx, newy, new_thetas[c], level-1)
  }

}

function animateTree(css_selector){
  if (typeof window.tree_animation_counter ==='undefined') 
    { window.tree_animation_counter = 0}
  else
    { window.tree_animation_counter++; }

  var selector=css_selector || "#d3_area1 svg g.line_group";
  var svg_element=d3.select(selector);
  svg_element.selectAll("line")
    .attr("transform",function(){
      var x1= d3.select(this).attr("x1");
      var y1= d3.select(this).attr("y1");
      return "rotate("+window.tree_animation_counter+","+x1+","+y1+")";
    })
    ;

}

function animate(seed){
    return function(){
    d3.select(this)
    .transition()
    .duration( function(){return seed*Math.random()*5000;})
    .attr("cx",function(){return seed*(Math.random()*700 + 50)} )
    .attr("cy",function(){return seed*(Math.random()*500 + 50)} )
    .style("fill", randColor )
    .attr("stroke",randColor)
    .attr("r",function(a){return (wfA.length/22)*a[1];})
    .each("end",animate(seed))
    ;
    }
  ;
}
$(document).ready(onLoad);